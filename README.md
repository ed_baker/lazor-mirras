Grid - The grid is a square composed of n x n blocks. A block is either empty or contains a mirror.

n x n grid

Mirror - Both faces of the mirror are reflective. They have two orientations - 45o left or right(see figure).

enter image description here

Light ray - It always travels straight and reflects from the mirror surface upon incidence (i=r=45o).

Code

Input - The user will input 3 values - n, m and s.

n represents the value of grid's length (no of blocks). You are free to choose the type of this value; It could be a string, integer, float or any other data type. This cannot be zero/empty.

m is a sequence of two values indicating where the mirrors are and what their orientations will be. You are free to choose the sequence type. For example, if 0 and 1 are the two orientations of the mirror and each block in the grid is marked from 1 to n2, you may input a sequence such as {(5,0), (3,1), (6,0),...}. Since you're free to choose this type, you may even squeeze them in to a string and decode it properly. In other words, it only needs to be legible to your code. This can be zero for no mirrors.

s represents the starting position of the light ray. Again, you may choose this type based on the rest of your code. Any form of data is valid.

NOTE The user is responsible for inputing the correct values. If they enter an invalid value such as placing a mirror out of the grid or not providing a starting position, your code doesn't have to deal with it.

Process - Calculate the position from which the light ray will emerge out of the grid. You are free to mark your grid (or not) in any way you like. The same goes with start and exit positions. Include these details in your answer below. How you represent these data will influence the output. See scoring rubrics for details.

Output - Display the emerging position of the light ray. Detailed output can score more. See scoring rubrics for details.