﻿namespace LAZORS.Domain
{
    public enum MirrorLean
    {
        Left = 0,
        Right = 1,
    }

    public enum MirrorReflection
    {
        Left = 0,
        Right = 1,
        Both = 2
    }

    public class Mirror
    {
        public Room Room { get; set; }
        public int Sides { get; set; }
        public MirrorLean Lean { get; set; }
        public MirrorReflection Reflection { get; set; }
    }
}
