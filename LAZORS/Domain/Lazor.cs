﻿namespace LAZORS.Domain
{
    public enum LazorDirection
    {
        Left = 0,
        Right = 1,
        Up = 2,
        Down = 3
    }

    public class Lazor
    {
        public int PosX { get; set; }
        public int PosY { get; set; }
        public LazorDirection Direction { get; set; }
    }
}
