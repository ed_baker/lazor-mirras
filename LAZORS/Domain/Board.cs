﻿using System.Collections.Generic;

namespace LAZORS.Domain
{
    public class Board
    {
        public int SizeX { get; set; }
        public int SizeY { get; set; }
        public List<Room> Rooms { get; set; }
    }
}