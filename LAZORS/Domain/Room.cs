﻿namespace LAZORS.Domain
{
    public class Room
    {
        public int PosX { get; set; }
        public int PosY { get; set; }
        public Mirror Mirror { get; set; }
    }
}