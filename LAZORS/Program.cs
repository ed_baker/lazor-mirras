﻿using System;
using System.Collections.Generic;
using LAZORS.Domain;
using LAZORS.Services;
using Microsoft.Practices.Unity;

namespace LAZORS
{
    class Program
    {
        private static IBuildor _buildor;
        private static ILazorMovor _lazorMovor;

        public Program()
        {
        }

        static void Main(string[] args)
        {
            // Declare a Unity Container
            var unityContainer = new UnityContainer();

            // Register so when dependecy is detected
            // it provides an instance
            unityContainer.RegisterType<IBuildor, Buildor>();
            unityContainer.RegisterType<ILazorMovor, LazorMovor>();

            _buildor = unityContainer.Resolve<Buildor>();
            _lazorMovor = unityContainer.Resolve<LazorMovor>();

            Board board = new Board();
            Lazor lazor = new Lazor();
            List<Mirror> mirrors = new List<Mirror>();

            string[] stringSeparators = new string[] { "\r\n" };

            // read in file
            var text = System.IO.File.ReadAllText(@"C:\Users\Ed\Desktop\Sample.txt").Split(stringSeparators, StringSplitOptions.None);

            var sectionBreak = 0;

            foreach (var line in text)
            {
                if (line == "-1")
                {
                    sectionBreak++;
                    continue;
                }

                if (sectionBreak == 0)
                {
                    var dimensions = line.Split(',');
                    board = _buildor.CreateBoard(Convert.ToInt32(dimensions[0]), Convert.ToInt32(dimensions[1]));
                }

                if (sectionBreak == 1)
                {
                    // create mirrors
                    mirrors.Add(_buildor.CreateMirror(line));
                }

                if (sectionBreak == 2)
                {
                    // LAZOR entry point
                    lazor = _buildor.CreateLazor(line);
                    break;
                }
            }

            // start the lazor movor
            _lazorMovor.AdvanceLazor(board, lazor);

            Console.ReadLine();
        }

    }

 
}