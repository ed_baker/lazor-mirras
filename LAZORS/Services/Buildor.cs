﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using LAZORS.Domain;

namespace LAZORS.Services
{
    public class Buildor : IBuildor
    {
        List<Room> RoomRepo = new List<Room>();
        List<Mirror> MirrorRepo = new List<Mirror>();

        public Lazor CreateLazor(string line)
        {
            var lazor = new Lazor();

            var lazorMatch = new Regex(@"^(?<x>\d+),(?<y>\d+)(?<line>[H|V])");
            Match match = lazorMatch.Match(line);

            var x = int.Parse(match.Groups["x"].Value);
            var y = int.Parse(match.Groups["y"].Value);
            var dir = match.Groups["line"].Value;

            lazor.PosX = x;
            lazor.PosY = y;

            var maxX = RoomRepo.OrderByDescending(i => i.PosX).FirstOrDefault().PosX;
            var maxY = RoomRepo.OrderByDescending(i => i.PosY).FirstOrDefault().PosY;

            if (dir == "H")
            {
                if (y == maxY)
                    lazor.Direction = LazorDirection.Left;
                else if (y == 0)
                    lazor.Direction = LazorDirection.Right;
            }
            else if (dir == "V")
            {
                if (x == maxX)
                    lazor.Direction = LazorDirection.Down;
                else if (x == 0)
                    lazor.Direction = LazorDirection.Up;
            }

            return lazor;
        }

        public Board CreateBoard(int x, int y)
        {
            return new Board
            {
                SizeX = x - 1,
                SizeY = y - 1,
                Rooms = CreateRoom(x, y)
            };
        }

        public List<Room> CreateRoom(int x, int y)
        {

            for (int i = 0; i <= x; i++)
            {
                for (int j = 0; j <= y; j++)
                {
                    RoomRepo.Add(new Room
                    {
                        PosX = i,
                        PosY = j
                    });
                }
            }

            return RoomRepo;
        }

        public Mirror CreateMirror(string line)
        {
            var mirror = new Mirror();

            var mirrorMatch = new Regex(@"^(?<x>\d+),(?<y>\d+)(?<line>[R|L]+)");
            Match match = mirrorMatch.Match(line);

            var x = int.Parse(match.Groups["x"].Value);
            var y = int.Parse(match.Groups["y"].Value);

            var room = RoomRepo.FirstOrDefault(i => i.PosX == x && i.PosY == y);

            // there will always be at least 1 char
            // 1 char = Lean position and both reflections
            mirror.Lean = match.Groups["line"].Value == "L" ? MirrorLean.Left : MirrorLean.Right;
            mirror.Sides = 2;
            mirror.Reflection = MirrorReflection.Both;
            mirror.Room = room;
            room.Mirror = mirror;

            if (match.Groups["line"].Value.Length == 2)
            {
                mirror.Reflection = match.Groups["line"].Value.Substring(1, 1) == "R" ? MirrorReflection.Right : MirrorReflection.Left;
                mirror.Sides = 1;
            }

            MirrorRepo.Add(mirror);

            return mirror;
        }
    }
}
