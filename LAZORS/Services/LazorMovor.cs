﻿using System;
using System.Linq;
using LAZORS.Domain;
using Microsoft.Practices.Unity;

namespace LAZORS.Services
{
    public class LazorMovor : ILazorMovor
    {
        public void AdvanceLazor(Board board, Lazor lazor)
        {
            do
            {
                Console.WriteLine("Lazor in: " + lazor.PosX + "," + lazor.PosY);

                var roomMirra =
                    board.Rooms.Where(i => i.PosX == lazor.PosX && i.PosY == lazor.PosY && i.Mirror != null)
                        .Select(i => i.Mirror)
                        .FirstOrDefault();

                if (roomMirra != null)
                {
                    Console.WriteLine("Room " + roomMirra.Room.PosX + "," + roomMirra.Room.PosY + " contains a mirra! ");

                    // which way is the mirror leaning?
                    if (roomMirra.Lean == MirrorLean.Right)
                    {
                        switch (roomMirra.Reflection)
                        {
                            case MirrorReflection.Right:
                                switch (lazor.Direction)
                                {
                                    case LazorDirection.Up:
                                        lazor.Direction = LazorDirection.Right;
                                        lazor.PosY++;
                                        break;
                                    case LazorDirection.Left:
                                        lazor.Direction = LazorDirection.Down;
                                        lazor.PosX--;
                                        break;
                                    case LazorDirection.Down:
                                        lazor.PosX--;
                                        break;
                                    case LazorDirection.Right:
                                        lazor.PosY++;
                                        break;
                                }
                                break;
                            case MirrorReflection.Left:
                                switch (lazor.Direction)
                                {
                                    case LazorDirection.Down:
                                        lazor.Direction = LazorDirection.Left;
                                        lazor.PosY--;
                                        break;
                                    case LazorDirection.Right:
                                        lazor.Direction = LazorDirection.Up;
                                        lazor.PosX++;
                                        break;
                                }
                                break;
                            default:
                                switch (lazor.Direction)
                                {
                                    case LazorDirection.Up:
                                        lazor.Direction = LazorDirection.Right;
                                        lazor.PosX++;
                                        break;
                                    case LazorDirection.Left:
                                        lazor.Direction = LazorDirection.Down;
                                        lazor.PosY--;
                                        break;
                                    case LazorDirection.Down:
                                        lazor.Direction = LazorDirection.Left;
                                        lazor.PosY--;
                                        break;
                                    case LazorDirection.Right:
                                        lazor.Direction = LazorDirection.Up;
                                        lazor.PosX++;
                                        break;
                                }
                                break;
                        }
                    }
                    else if (roomMirra.Lean == MirrorLean.Left)
                    {
                        switch (roomMirra.Reflection)
                        {
                            case MirrorReflection.Right:
                                switch (lazor.Direction)
                                {
                                    case LazorDirection.Down:
                                        lazor.Direction = LazorDirection.Right;
                                        lazor.PosY++;
                                        break;
                                    case LazorDirection.Left:
                                        lazor.Direction = LazorDirection.Up;
                                        lazor.PosX++;
                                        break;
                                }
                                break;
                            case MirrorReflection.Left:
                                switch (lazor.Direction)
                                {
                                    case LazorDirection.Up:
                                        lazor.Direction = LazorDirection.Left;
                                        lazor.PosY--;
                                        break;
                                    case LazorDirection.Right:
                                        lazor.Direction = LazorDirection.Down;
                                        lazor.PosX--;
                                        break;
                                }
                                break;
                            default:
                                switch (lazor.Direction)
                                {
                                    case LazorDirection.Down:
                                        lazor.Direction = LazorDirection.Right;
                                        lazor.PosY++;
                                        break;
                                    case LazorDirection.Up:
                                        lazor.Direction = LazorDirection.Left;
                                        lazor.PosY--;
                                        break;
                                    case LazorDirection.Left:
                                        lazor.Direction = LazorDirection.Up;
                                        lazor.PosX++;
                                        break;
                                    case LazorDirection.Right:
                                        lazor.Direction = LazorDirection.Down;
                                        lazor.PosX--;
                                        break;
                                }
                                break;
                        }
                    }
                }
                else
                {
                    Console.WriteLine("No mirra hurr. Advance.");
                    // don't update direction, just x/y
                    switch (lazor.Direction)
                    {
                        case LazorDirection.Down:
                            lazor.PosX--;
                            break;
                        case LazorDirection.Up:
                            lazor.PosX++;
                            break;
                        case LazorDirection.Left:
                            lazor.PosY--;
                            break;
                        case LazorDirection.Right:
                            lazor.PosY++;
                            break;
                    }
                }
            } while (board.Rooms.Any(i => i.PosX == lazor.PosX && i.PosY == lazor.PosY));

            Console.WriteLine("U done!");

            // advance oneself 
        }
    }
}
