﻿using LAZORS.Domain;

namespace LAZORS.Services
{
    public interface ILazorMovor
    {
        void AdvanceLazor(Board board, Lazor lazor);
    }
}
