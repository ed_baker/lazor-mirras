﻿using System.Collections.Generic;
using LAZORS.Domain;

namespace LAZORS.Services
{
    public interface IBuildor
    {
        Lazor CreateLazor(string line);
        Board CreateBoard(int x, int y);
        List<Room> CreateRoom(int x, int y);
        Mirror CreateMirror(string line);

    }
}
